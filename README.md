# Floating Fortress - Wireless Charging 3
This is the Gitlab project page for the 3rd semester project 3 charging system for underwater unmanned drone in connection with the company **Copenhagen Robots** with
collaboration with 1st semeter class I.T Technology students and lecturers of **UCL - University College**. 

## Project Description
The whole project agreement between the mentioned parties in the introduction can be found within the project repository under 
**"Project_Management/Project_Agreement/Wireless_Power_Transfer_and_Charging_for_Mobile_Vehicles.docx"**. The overall description of the project and the expected roles of all
parties are defined with that document, but in short, the given project is a task to create a floating charging platform which should be able to deploy on a small lake and 
keep an underwater drone charged with power, without running any cables from our "Raft" to the given drone, or from land to the raft.

### Need to Fill
This section is coming soon.

## Merge Requests

## Branch Handling

## Hardware Review
When working with a new form of hardware or circuitry for the project it's expected that the members who are working with the device/components/circuitry should perform a "Hardware
review" to ensure that the elements that they are working with works as intended. For the full process of the hardware review look up under the "Hardware Review Process" right
under here with this section. After the review has been completed a few resources from the review, it is expected to be uploaded to a designated directory with a set of rulesets of
how the different files should be named, and placed within that directory.

### Hardware Review Process
When reviewing a device/components/circuitry for usage in the project, a few prerequisites and guidelines should be achieved before starting the hardware review These achievements being that
the device/components/curcity should not have been mishandled in any way before preforming the hardware review. It's also expected that the person or persons performing
the review are connected with **ESD** (Electrostatic Discharge) secure equipment or is in any way grounded. It's expected that you have a USB-SATA cable to perform the review.

**Review Process**
The Hardware Review should only be preformed in **E-LAB** (**Electronics Laboratory**) in **UCL - University College** facilities or an environment which meets the standard and 
qualifications equal to that laboratory.
1.  **Preparation 1/2** - Make sure that every precaution have been meet, and all of the needed items are within range of you.
2.  **Preparation 2/2** - Connect yourself with a anti-static wrist strap, abd turn on the digital microscope and insert your USB-Stick into it.
3.  **Review 1/3** - Analyse & Identify the relvant information about the given item under review.
4.  **Review 2/3** - If there is any interesting elements of the item take a image of the element, and save it to the USB Stick.
5.  **Review 3/3** - If the item have any functionally, this functionally should be tested if possiable.
6.  **Documentation 1/2** - Rename the images acording to Standards **"Item_Name ID"**, and upload them to **"Documentation/Hardware Review/Item_Name/"**.
7.  **Documentation 2/2** - Create a small note file about the different elements of the item, and it's condition in the same folder as the images.

**Hardware Review Notice** - Hardware Reviews should be preformed before any kind of other actions can be preformed with the Item / Device.

## Issue Standards
Within the section is the expected standards for Issues related to this project, if these standards are not met, please revisit the given issues and update them until these 
standards are met. Issues related to this project have a few elements that must be followed before being considered a valid issue. These elements are listed with the 
list called **"Issue Standard Elements"** below, if an issue is lacking any one of these element the creator will be noted and contacted about the given issue with a "Misconfigured element" label and if the issue is not edited within a week, the issue will be terminated and marked with a "Failed" label.

### Issue Standard Elements
Below is the list of expected elements that every new issue created after 25/09/2019 should fulfill, before the issue should be handled by anyone. If these elements are not
applied by anyone, please follow the needed guidelines for **"Problems Related to Issues"** under the **"Problem Handling"** Section.

1. **Issue Name** - Please capitalise all new issues titles with the following capitalisation rules from the given examples: **[E.g 01 Charge Transfer to Drone]** **[E.g 02 Retyping of Issues]**.
2. **Issue Description** - Please give every new issue a good description with all of the needed details to continue work on the given issue.
3. **Issue Requirements** - Please provide every issues with all of the needed requirements, guidelines, or subtasks that are needed to be completed before the issue is done.
4. **Issue Labeling** - Please give every issue a label or more related to "State of issue", "Type of work" or "Other/Special".
5. **Issue Placement of Work** - If the work of the issue is being shared or given to other members please provide an expected location for the work within the follow guildelines **"Floder/Sub_Folder/File_With_Underscore_As_Space.Extension"**.
6. **Issue Notice** - If there are any special elements connected to an issue, please define or inform connected members with a **"Note to Issue"** section just alike the one in **"First Draft of Design Plans for the Raft" - Issue #06**.
7. **Issue Deadline** - Please try to add a realistic deadline to every issue.
8. **Issue Common Language** - Please make the language of every issue in English (UK/US).
9. **Issue Link** - If there are any related resource to an issue please provide a link the resource.

**Issue Deletion Notice** - Issues are **NOT** Deleted, only closed by Two Maintainers by the projects.

## Label Collection and Association
Within this section is a detailed list over all the used Labels used in this Gitlab project, and when to use the different labels at the right times any misuse related to Labels
and Label use will also be handled under the **"Problems related to Labels"** under the **"Problem Handling"** Section.

**Label Collection**

1.  **Done** - This label indicates that the element which it's relates to is done, and is expected to be closed.
2.  **Paused** - This label indicates that the issue is indeterminable amount of time.
3.  **Failed** - This label is applied when a task or element failed in a overall fashion, and is expected to be closed.
4.  **Open** - This label indicates that the element is open for anyone to assign themselves.
5.  **In Progess** - This label indicates that it's currently being work on by a member.
6.  **Awaiting Assignment** - This label is applied when a issue is open and is waiting Awaiting Assignment by anyone or a specific member.
7.  **Awaiting Peer Review** - This label requests a peer review of the given issue, so the issue can be closed.
8.  **Requesting Support** - This label requests active support from one or more members.
9.  **Requesting Information** - This label is requests specific information on a subject before any work can continue.
10. **Misconfigured Element** - This label indicates that the element does not uphold the given standards and should be revised by the creator.
11. **Over-Due** - This label states that the currently issue is over-due the deadline.
12. **Critical** - This label states that the connected element is critical project.


## Problem Handling

When one or more problems materialize it can be hard to find the right way to handle them in the proper way, so this section is for that purpose. This section is split
into a few sub-sections to different related project problems, if you are looking for help to member related problems please contact a teacher or a project member, also
if this section does not solve your problem, then please contact a project member by one of the standard communication ways.

### Problems Related to Hardware Review

### Problems Related to Issues

### Problems related to Labels

## Q&A