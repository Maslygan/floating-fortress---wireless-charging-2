Hardware Review Notes - PiJUice 40 Watt Solar Panel 28/09/2019.

The overall condition of the solar panel is fine, and seem to be any problems 
or damages that we have to attend to in the future. the output from the solar 
panel seems to be steady and stable enough so that we can have power from the 
sun without any problems.

The only thing to take of note related to this solar panel is that the fabrics
take easy shape of the way of folding the panel itself which could be a problem
if the panel is moved around a lot.

This item is approve for further testing on the 13/10/2019.
Hardware Review was performed by Thais W. Nielsen on 13/10/2019.