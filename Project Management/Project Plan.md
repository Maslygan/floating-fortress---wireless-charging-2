---
title: 'Floating Fortress - Wireless Charging 2'
subtitle: 'Project Plan'
authors: ['Thomas Bargisen thom8723@edu.eal.dk']
date: \today
left-header: \today
right-header: Project Plan
skip-toc: false
---



# Background

The projects goal is to develop a solution for wireless charging of robots. All terrain types, will be considered.
Land, water and air. Students from the first and third semester of UCLs study line IT-Technologies will be the ones developing the solutions.
Ilias Esmati and Ruslan Trifonov will be supervisor and technical leads. A renewable energy solution will be the prime method of delivering electricity.
The projects outcome will be considers success if the following goals are delivered.

-	Renewable source of electric
-	power transformation and storage
-	wireless power transfer
- charging and storing power on target.

# Goals

Project deliveries are:
-

# Schedule

TimeEdit, we currently have moday Scheduled as the only project day of the week.


# Organization
## Group Members
- Thomas Bargisen
- Jesper Aggerholm
- Thais Nielsen
- Adnan Radenica
- Kasper Jensen
- Daniel Maslygan

## Advisors / Overseers
- Ilias Esmati
- Ruslan Trifonov

## External resources
- Copenhagen robotics

### Knowledge
- Classmates
- Teachers

For each of the identified groups and people, their tasks must be specified and their role in the project must be clear. This could be done in relation to the goals of the project.]


# Budget and resources

Components will be provided by UCL. If special components are required a proposal will be made to Copenhagen Technologies to share the expenses.


# Risk assessment

Risk assessment is a list of possible risks to the project, and suggestions on how to solve them.

## Project risks

- Since the project involves water and electronics, ensuring that everything stays dry can be a challenge.
- Making sure that we can actually successfully transfer power wirelessly when submarine is in water.

## How to assess risks properly.

## Teamwork

- Isolated, not communicating what you are doing: Talk to team if you can't figure something out. If you run your own game, you hurt the team.

## Documentation

-g

## Project Management

- Make sure to keep group meetings, a regular thing. So all are on the same page!
- Never leave issues unresolved! make priorities

## Knowledge

- Seek knowledge / help from class mates.
- Be honest with the goals, never hide your progress!
- Reduce scope of the tasks/project


## Motivation.

- If you lack the motivation, talk with the team. find a solution!

## Electrical Failures

- Make backups
- Spare parts
- Triple check everything
- Don't make high risk decisions

## Sick Leave

- Keep contact
- Redistribute workflow
- Try to work from home


# Stakeholders

The stakeholders include everyone that is actively involved in the project

## Group members

Actively working on tasks to complete the project

| **Names**       | **Email**           | **Gitlab**      |
|-----------------|---------------------|-----------------|
| Thomas Bargisen | thom8723@edu.ucl.dk | ducky-chan      |     
| Jesper Aggerholm| jesp9795@edu.ucl.dk | jesperaggerholm |
| Adnan Radenica  | adna0783@edu.ucl.dk | beefboi
| Thais Nielsen   | thai0051@edu.ucl.dk | thaiswnielsen   |
| Kasper Jensen   | kasp7547@edu.ucl.dk | kasp7547        |
| Daniel Maslygan | dani7378@edu.ucl.dk | maslygan        |

## Project coordinators
Actively overseeing the project
- Ilias Esmati
- Ruslan Trifonov


# Communication

## Group members
All communication within the group will take place via Discord, Messenger or other messaging applications


## Project coordinators
Communication to and from the project coordinators will manifest mainly via weekly meetings, itslearning and e-mails

# References

None at this time
