# Team Meeting 

Within this "README.md" file is for keeping record of the team meeting 
notes and when the notes was taken. There should be a writen record of 
every team meeting that have been held.

## Team Meeting Record

Within this section is a numbered list of every record of files from 
the different team meetings. Every entry within this list should contain
the following information (**Index|FIle Name|Description|File Location|Date|Member Name**).

| Index  | File Name | Description | File Location | Date | Member Name |
| ------ | ------ | ------ | ------ | ------ | ------ |
| 1 | Teacher_Meetings_16-06-2019.txt | Starting out at the research state.     | /Meeting Notes-16_06_2019/ | 16/06/2019 | Thais W. Nielsen |
| 2 | Team_Meetings__07-10-2019.txt   | Transformation to the deveopment state. | /Meeting Notes-07_10_2019/ | 07/10/2019 | Thais W. Nielsen |
| 3 | Team_Meeting_21-10-2019.txt     | Back from holiday, and keep attendance. | /Meeting Notes-21_10_2019/ | 21/10/2019 | Thais W. Nielsen |
| 4 | Team_Meeting_28-10-2019.txt     | Redefing the tests.                     | /Meeting Notes-28_10_2019/ | 28/10/2019 | Thais W. Nielsen |

This record should be updated by the 3rd semsemters which is holding the meeting with the 1st semesters,
these records should be keept updated onces a week after the team meeting on each Monday of the week. On 
holidays and/or on days where there is no planned team meeting, this records will not be updated. 